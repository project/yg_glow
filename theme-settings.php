<?php

/**
 * @file
 * Provides an additional config form for theme settings.
 */

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_system_theme_settings_alter().
 */
function yg_glow_form_system_theme_settings_alter(array &$form, FormStateInterface $form_state) {
  $form['visibility'] = [
    '#type' => 'vertical_tabs',
    '#title' => t('YG Glow Settings'),
    '#weight' => -999,
  ];
  $form['social'] = [
    '#type' => 'details',
    '#attributes' => [],
    '#title' => t('Social Links'),
    '#weight' => -999,
    '#group' => 'visibility',
    '#open' => FALSE,
  ];
  // Social links.
  $form['social']['social_links'] = [
    '#type' => 'details',
    '#title' => t('Contact Info'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];
  $form['social']['social_links']['social_title'] = [
    '#type' => 'textfield',
    '#title' => t('Social Section Title'),
    '#description' => t('Please enter social section title'),
    '#default_value' => theme_get_setting('social_title'),
    '#required' => TRUE,
  ];
  $form['social']['social_links']['twitter_url'] = [
    '#type' => 'textfield',
    '#title' => t('Twitter'),
    '#description' => t('Please enter your twitter url'),
    '#default_value' => theme_get_setting('twitter_url'),
  ];
  $form['social']['social_links']['facebook_url'] = [
    '#type' => 'textfield',
    '#title' => t('Facebook'),
    '#description' => t('Please enter your facebook url'),
    '#default_value' => theme_get_setting('facebook_url'),
  ];
  $form['social']['social_links']['youtube_url'] = [
    '#type' => 'textfield',
    '#title' => t('Youtube'),
    '#description' => t('Please enter your youtube url'),
    '#default_value' => theme_get_setting('youtube_url'),
  ];
  $form['social']['social_links']['instagram_url'] = [
    '#type' => 'textfield',
    '#title' => t('Instagram'),
    '#description' => t('Please enter your instagram url'),
    '#default_value' => theme_get_setting('instagram_url'),
  ];

  // Footer.
  $form['footer_about'] = [
    '#type' => 'details',
    '#attributes' => [],
    '#title' => t('About Footer Section'),
    '#weight' => -998,
    '#group' => 'visibility',
    '#open' => FALSE,
  ];
  $form['footer_about']['about'] = [
    '#type' => 'details',
    '#title' => t('Footer'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];
  $form['footer_about']['about']['about_title'] = [
    '#type' => 'textfield',
    '#title' => t('About Title'),
    '#description' => t('Please enter footer about section title'),
    '#default_value' => theme_get_setting('about_title'),
    '#required' => TRUE,
  ];
  $form['footer_about']['about']['about_desc'] = [
    '#type' => 'textarea',
    '#title' => t('About Description'),
    '#description' => t('Please enter footer about section description'),
    '#default_value' => theme_get_setting('about_desc'),
  ];
  $form['footer'] = [
    '#type' => 'details',
    '#attributes' => [],
    '#title' => t('Footer Section'),
    '#weight' => -997,
    '#group' => 'visibility',
    '#open' => FALSE,
  ];
  $form['footer']['copyright'] = [
    '#type' => 'text_format',
    '#title' => t('Copyrights'),
    '#default_value' => theme_get_setting('copyright')['value'],
    '#description'   => t("Please enter the copyright content here."),
  ];

}
